package az.ingress.Microservice14;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Microservice14Application {

	public static void main(String[] args) {
		SpringApplication.run(Microservice14Application.class, args);
	}

}
